FROM openshift/jenkins-2-centos7
MAINTAINER Dominik Schroeter <dominik.schroeter@bmw.de>

COPY plugins.txt /opt/openshift/configuration/plugins.txt
RUN /usr/local/bin/install-plugins.sh /opt/openshift/configuration/plugins.txt

USER root

COPY init_scripts/src/main/groovy/ /opt/openshift/configuration/init.groovy.d/
RUN  /usr/local/bin/fix-permissions /opt/openshift/configuration/init.groovy.d/

USER 1001

