#! /bin/bash

rm -rf target/
mkdir -p target/octool
mkdir -p target/octool/configuration/init.groovy.d

cp init_scripts/src/main/groovy/*.groovy target/octool/configuration/init.groovy.d
cp config.xml.tpl target/octool/configuration
cp plugins.txt target/octool
s2i build target/octool openshift/jenkins-2-centos7 my-jenkins
